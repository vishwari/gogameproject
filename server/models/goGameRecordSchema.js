var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var database = require('../config/database')
autoIncrement = require('mongoose-auto-increment');
var connection = mongoose.createConnection(database.database);
autoIncrement.initialize(connection);

var goGameRecordSchema = new Schema({
  userRecordId: {
    type: Number,
    unique: true
  },
  gameName: {
    type: String,
  },
  score: {
    type: String,
  },
  date: {
    type: String,
  },
  userName: {
    type: String,
  },
  createdTime:{
    type: String,
  },
  modifiedDate:{
    type: String,
  },
  modifiedTime:{
    type: String,
  }
});

goGameRecordSchema.plugin(autoIncrement.plugin, {
  model: "goGameRecordModal",
  field: 'userRecordId',
  startAt: 1,
});

var goGameRecordModal = module.exports = mongoose.model('goGameRecord', goGameRecordSchema);
