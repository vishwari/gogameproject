const express = require('express');
const router = express.Router();
var app = express();
var goGameRecordSchema = require('../models/goGameRecordSchema')


router.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
  next();
});


/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Express' });
});

//create records

router.post('/createUserRecord', (req, res) => {
  console.log("coming inside", req.body)
  var date_ob = new Date();
  var date = ("0" + date_ob.getDate()).slice(-2);
  var month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
  var year = date_ob.getFullYear();
  var hours = date_ob.getHours();
  var minutes = date_ob.getMinutes();
  var seconds = date_ob.getSeconds();
  var dateNew = date + "/" + month + "/" + year;
  var timeNew = hours + ":" + minutes + ":" + seconds;
  var dateNTime = date + "/" + month + "/" + year + " " + hours + ":" + minutes + ":" + seconds;
  console.log("sysytem date and time", dateNTime);

  var GoGameRecordSchema = new goGameRecordSchema({
    gameName: req.body.gameName,
    userName: req.body.userName,
    date: dateNew,
    score: req.body.score,
    createdTime: timeNew,
  })

  GoGameRecordSchema.save(function (err, result) {
    if (err) {
      console.log("err", err)
      res.json({ error: err });
    }
    else {
      res.json({ result: result });
    }
  })

});

//get all records
router.get('/getUserRecord', (req, res) => {
  goGameRecordSchema.find(function (err, result) {
    if (err) {
      console.log("err", err)
      return err;
    }
    else {
      return res.json({ result: result })
    }

  })
});

// update the Records

router.put('/editRecord', (req, res, callback) => {
  var date_ob = new Date();
  var date = ("0" + date_ob.getDate()).slice(-2);
  var month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
  var year = date_ob.getFullYear();
  var hours = date_ob.getHours();
  var minutes = date_ob.getMinutes();
  var seconds = date_ob.getSeconds();
  var dateNew = date + "/" + month + "/" + year;
  var timeNew = hours + ":" + minutes + ":" + seconds;
  var dateNTime = date + "/" + month + "/" + year + " " + hours + ":" + minutes + ":" + seconds;
  console.log("sysytem date and time", dateNTime);

  let query = {
    "userRecordId": req.body.userRecordId,
  }
  let query1 = {
    "gameName": req.body.gameName,
    "userName": req.body.userName,
    "date": dateNew,
    "score": req.body.score,
    "createdTime": timeNew,
    "modifiedDate": dateNew,
    "modifiedTime": timeNew
  }
  goGameRecordSchema.findOneAndUpdate(query, { $set: query1 }, { new: true }, (err, result) => {
    if (!err) {
      if (result == null) {
        res.json({
          res: "Record NOt Found"
        })
      }
      else {
        res.json({
          res: "Record Successfully Updated"
        })
      }
    } else {
      console.log("err", err)
      return err;
    }
  })
})

router.delete('/deleteRecord', (req, res, callback) => {
  goGameRecordSchema.deleteOne({ userRecordId: req.query.userRecordId }, function (err,result) {
    if (!err) {
      if (result == null) {
        res.json({
          res: "Record Not Found"
        })
      }
      else {
        res.json({
          res: "Record Successfully Deleted"
        })
      }
    } else {
      console.log("err", err)
      return err;
    }
});

});

module.exports = router;
